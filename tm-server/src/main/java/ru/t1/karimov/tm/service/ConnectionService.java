package ru.t1.karimov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.property.IDatabaseProperty;
import ru.t1.karimov.tm.api.service.IConnectionService;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String username = databaseProperty.getDatabaseUsername();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseUrl();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
