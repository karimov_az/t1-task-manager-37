package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.IRepository;
import ru.t1.karimov.tm.comparator.CreatedComparator;
import ru.t1.karimov.tm.comparator.StatusComparator;
import ru.t1.karimov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected static final String COLUMN_ID = "id";

    @NotNull
    protected static final String COLUMN_CREATED = "created";

    @NotNull
    protected final Connection connection;

    protected AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    @NotNull
    public abstract M fetch(@NotNull final ResultSet resultSet) throws Exception;

    @NotNull
    public abstract M add(@NotNull final M model) throws Exception;

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) throws Exception {
        @NotNull List<M> result = new ArrayList<>();
        for (@NotNull final M model : models) result.add(add(model));
        return result;
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String sql = String.format("SELECT COUNT(1) FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.next()) return 0;
            return resultSet.getInt("count");
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }

    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) throws Exception {
        return findAll().get(index);
    }

    @NotNull
    @Override
    public M removeOne(@NotNull final M model) throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@NotNull final String id) throws Exception {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@NotNull final Integer index) throws Exception {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(query);
        }
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) throws Exception {
        removeAll();
        return add(models);
    }

}
