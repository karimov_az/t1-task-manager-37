package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String TABLE_PROJECT = "tm_project";

    @NotNull
    private static final String COLUMN_NAME = "name";

    @NotNull
    private static final String COLUMN_DESCRIPTION = "description";

    @NotNull
    private static final String COLUMN_STATUS = "status";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_PROJECT;
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project model) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)",
                getTableName(), COLUMN_ID, COLUMN_NAME, COLUMN_CREATED, COLUMN_DESCRIPTION, COLUMN_USER_ID, COLUMN_STATUS
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getName());
            statement.setTimestamp(3, new Timestamp(model.getCreated().getTime()));
            statement.setString(4, model.getDescription());
            statement.setString(5, model.getUserId());
            statement.setString(6, model.getStatus().toString());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    public Project add(@NotNull final String userId, @NotNull final Project model) throws Exception {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public Project fetch(@NotNull ResultSet resultSet) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(resultSet.getString(COLUMN_ID));
        project.setCreated(resultSet.getTimestamp(COLUMN_CREATED));
        project.setName(resultSet.getString(COLUMN_NAME));
        project.setCreated(resultSet.getTimestamp(COLUMN_CREATED));
        project.setDescription(resultSet.getString(COLUMN_DESCRIPTION));
        project.setUserId(resultSet.getString(COLUMN_USER_ID));
        project.setStatus(Status.toStatus(resultSet.getString(COLUMN_STATUS)));
        return project;
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public Project update(@NotNull final Project project) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), COLUMN_NAME, COLUMN_DESCRIPTION, COLUMN_STATUS, COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getId());
            statement.executeUpdate();
        }
        return project;
    }

}
