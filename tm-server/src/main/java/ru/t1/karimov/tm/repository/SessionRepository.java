package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.repository.ISessionRepository;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @NotNull
    private static final String TABLE_SESSION = "tm_session";

    @NotNull
    private static final String COLUMN_DATE = "date";

    @NotNull
    private static final String COLUMN_ROLE = "role";

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_SESSION;
    }

    @NotNull
    @Override
    public Session add(@NotNull Session model) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)",
                getTableName(), COLUMN_ID, COLUMN_DATE, COLUMN_USER_ID, COLUMN_ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getDate().getTime()));
            statement.setString(3, model.getUserId());
            statement.setString(4, model.getRole().toString());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    public Session add(@NotNull String userId, @NotNull Session model) throws Exception {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public Session fetch(@NotNull ResultSet resultSet) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(resultSet.getString(COLUMN_ID));
        session.setCreated(resultSet.getTimestamp(COLUMN_CREATED));
        session.setDate(resultSet.getTimestamp(COLUMN_DATE));
        session.setUserId(resultSet.getString(COLUMN_USER_ID));
        session.setRole(Role.valueOf(resultSet.getString(COLUMN_ROLE)));
        return session;
    }

    @NotNull
    @Override
    public Session update(@NotNull final Session session) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), COLUMN_DATE, COLUMN_USER_ID, COLUMN_ROLE, COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(session.getDate().getTime()));
            statement.setString(2, session.getUserId());
            statement.setString(3, session.getRole().toString());
            statement.setString(4, session.getId());
            statement.executeUpdate();
        }
        return session;
    }

}
