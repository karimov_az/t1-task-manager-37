package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model) throws Exception;

    @NotNull
    List<M> findAll(@NotNull String userId) throws Exception;

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index) throws Exception;

    boolean existsById(@NotNull String userId, @NotNull String id) throws Exception;

    int getSize(@NotNull String userId) throws Exception;

    @Nullable
    M removeOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    M removeOneByIndex(@NotNull String userId, @NotNull Integer index) throws Exception;

    @Nullable
    M removeOne(@NotNull String userId, @NotNull M model) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

}
