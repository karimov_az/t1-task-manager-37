package ru.t1.karimov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private static final String USER_TABLE = "tm_user";

    @NotNull
    private static final String COLUMN_ID = "id";

    @NotNull
    private static final String COLUMN_LOGIN = "login";

    @NotNull
    private static final String COLUMN_PASSWORD = "password";

    @NotNull
    private static final String COLUMN_EMAIL = "email";

    @NotNull
    private static final String COLUMN_LOCKED = "locked";

    @NotNull
    private static final String COLUMN_FIRST_NAME = "first_name";

    @NotNull
    private static final String COLUMN_LAST_NAME = "last_name";

    @NotNull
    private static final String COLUMN_MIDDLE_NAME = "middle_name";

    @NotNull
    private static final String COLUMN_ROLE = "role";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return USER_TABLE;
    }

    @NotNull
    @Override
    public User add(@NotNull final User model) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName(), COLUMN_ID, COLUMN_LOGIN, COLUMN_PASSWORD, COLUMN_EMAIL, COLUMN_LOCKED,
                COLUMN_FIRST_NAME, COLUMN_LAST_NAME, COLUMN_MIDDLE_NAME, COLUMN_ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getLogin());
            statement.setString(3, model.getPasswordHash());
            statement.setString(4, model.getEmail());
            statement.setBoolean(5, model.getLocked());
            statement.setString(6, model.getFirstName());
            statement.setString(7, model.getLastName());
            statement.setString(8, model.getMiddleName());
            statement.setString(9, model.getRole().toString());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    public User fetch(@NotNull final ResultSet resultSet) throws Exception {
        @NotNull final User user = new User();
        user.setId(resultSet.getString(COLUMN_ID));
        user.setCreated(resultSet.getTimestamp(COLUMN_CREATED));
        user.setLogin(resultSet.getString(COLUMN_LOGIN));
        user.setPasswordHash(resultSet.getString(COLUMN_PASSWORD));
        user.setEmail(resultSet.getString(COLUMN_EMAIL));
        user.setLocked(resultSet.getBoolean(COLUMN_LOCKED));
        user.setFirstName(resultSet.getString(COLUMN_FIRST_NAME));
        user.setLastName(resultSet.getString(COLUMN_LAST_NAME));
        user.setMiddleName(resultSet.getString(COLUMN_MIDDLE_NAME));
        user.setRole(Role.valueOf(resultSet.getString(COLUMN_ROLE)));
        return user;
    }

    @Override
    public void update(@NotNull final User model) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), COLUMN_LOGIN, COLUMN_PASSWORD, COLUMN_EMAIL, COLUMN_LOCKED,
                COLUMN_FIRST_NAME, COLUMN_LAST_NAME, COLUMN_MIDDLE_NAME, COLUMN_ROLE, COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getLogin());
            statement.setString(2, model.getPasswordHash());
            statement.setString(3, model.getEmail());
            statement.setBoolean(4, model.getLocked());
            statement.setString(5, model.getFirstName());
            statement.setString(6, model.getLastName());
            statement.setString(7, model.getMiddleName());
            statement.setString(8, model.getRole().toString());
            statement.setString(9, model.getId());
            statement.executeUpdate();
        }
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(), COLUMN_LOGIN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(), COLUMN_EMAIL);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }

    }

    @Override
    public boolean isEmailExist(@NotNull final String email) throws Exception {
        return findByEmail(email) != null;
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) throws Exception {
        return findByLogin(login) != null;
    }

}
