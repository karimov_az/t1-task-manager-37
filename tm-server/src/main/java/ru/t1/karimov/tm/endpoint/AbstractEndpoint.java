package ru.t1.karimov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.service.IServiceLocator;
import ru.t1.karimov.tm.dto.request.AbstractUserRequest;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.EndpointException;
import ru.t1.karimov.tm.exception.user.AccessDeniedException;
import ru.t1.karimov.tm.model.Session;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected Session check(@Nullable final AbstractUserRequest request) throws AbstractException {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        try {
            return serviceLocator.getAuthService().validateToken(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }

    }

    @NotNull
    protected Session check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) throws AbstractException {
        if (role == null) throw new AccessDeniedException();
        @NotNull final Session session = check(request);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

}
