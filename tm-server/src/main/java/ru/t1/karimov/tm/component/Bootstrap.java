package ru.t1.karimov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.endpoint.*;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.api.repository.ISessionRepository;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.service.*;
import ru.t1.karimov.tm.endpoint.*;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.entity.UserNotFoundException;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.model.User;
import ru.t1.karimov.tm.repository.ProjectRepository;
import ru.t1.karimov.tm.repository.SessionRepository;
import ru.t1.karimov.tm.repository.TaskRepository;
import ru.t1.karimov.tm.service.*;
import ru.t1.karimov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final Connection connection = connectionService.getConnection();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connection);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(connection);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(
            connectionService, projectRepository, taskRepository, propertyService
    );

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository(connection);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void initDemoData() throws Exception {
        @NotNull final String demoLoad = propertyService.getDataDemoLoad();
        if ("n".equals(demoLoad)) return;

        if (userService.findByLogin("test") == null) {
            userService.create("test", "test", "test@test.ru");
        }
        if (userService.findByLogin("user") == null) {
            userService.create("user", "user", "user@user.ru");
        }
        if (userService.findByLogin("admin") == null) {
            userService.create("admin","admin", Role.ADMIN);
        }

        @Nullable final User user1 = userService.findByLogin("test");
        @Nullable final User user2 = userService.findByLogin("user");
        @Nullable final User user3 = userService.findByLogin("admin");
        if (user1 == null) throw new UserNotFoundException();
        if (user2 == null) throw new UserNotFoundException();
        if (user3 == null) throw new UserNotFoundException();
        @NotNull final String user1_id = user1.getId();
        @NotNull final String user2_id = user2.getId();
        @NotNull final String user3_id = user3.getId();

        projectService.add(new Project(user1_id,"Project_01", "Desc_01_user 1", Status.IN_PROGRESS));
        projectService.add(new Project(user1_id, "Project_02", "Desc_02_user 1", Status.NOT_STARTED));
        projectService.add(new Project(user1_id, "Project_03", "Desc_03_user 1", Status.IN_PROGRESS));
        projectService.add(new Project(user1_id, "Project_04", "Desc_04_user 1", Status.COMPLETED));
        projectService.add(new Project(user2_id,"Project_01", "Desc_01_user 2", Status.IN_PROGRESS));
        projectService.add(new Project(user2_id, "Project_02", "Desc_02_user 2", Status.NOT_STARTED));
        projectService.add(new Project(user3_id, "Project_01", "Desc_01_user 3", Status.IN_PROGRESS));
        projectService.add(new Project(user3_id, "Project_02", "Desc_02_user 3", Status.COMPLETED));

        final String projectId1 = projectService.findOneByIndex(user1_id,3).getId();
        final String projectId2 = projectService.findOneByIndex(user2_id,1).getId();
        final String projectId3 = projectService.findOneByIndex(user3_id,1).getId();

        taskService.add(new Task(user1_id, "Task_01", "Desc task 1 user 1", Status.IN_PROGRESS, null));
        taskService.add(new Task(user1_id, "Task_02", "Desc task 2 user 1", Status.NOT_STARTED, null));
        taskService.add(new Task(user1_id, "Task_03", "Desc task 3 user 1", Status.COMPLETED, projectId1));
        taskService.add(new Task(user1_id, "Task_04", "Desc task 4 user 1", Status.NOT_STARTED, projectId1));
        taskService.add(new Task(user2_id, "Task_01", "Desc task 1 user 2", Status.IN_PROGRESS, projectId2));
        taskService.add(new Task(user2_id, "Task_02", "Desc task 2 user 2", Status.NOT_STARTED, null));
        taskService.add(new Task(user3_id, "Task_01", "Desc task 1 user 3", Status.COMPLETED, projectId3));
        taskService.add(new Task(user3_id, "Task_01", "Desc task 2 user 3", Status.NOT_STARTED, null));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String port = getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    public void start() throws Exception {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

}
