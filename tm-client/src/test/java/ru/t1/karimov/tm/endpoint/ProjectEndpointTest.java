package ru.t1.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.karimov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.request.project.*;
import ru.t1.karimov.tm.dto.request.user.UserLoginRequest;
import ru.t1.karimov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.karimov.tm.dto.response.project.*;
import ru.t1.karimov.tm.dto.response.user.UserLoginResponse;
import ru.t1.karimov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.karimov.tm.enumerated.ProjectSort;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.marker.SoapCategory;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.service.PropertyService;

import java.util.List;

import static org.junit.Assert.*;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Nullable
    private String token;

    @Nullable
    private Project project;

    @Before
    public void initTest() throws Exception {
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        token = userLoginResponse.getToken();
        projectEndpoint.clearProjects(new ProjectClearRequest(token));
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        projectCreateRequest.setName("Project 1");
        projectCreateRequest.setDescription("Project Description 1");
        @NotNull final ProjectCreateResponse projectCreateResponse = projectEndpoint.createProject(
                projectCreateRequest
        );
        project = projectCreateResponse.getProject();
    }

    @After
    public void initEndTest() throws Exception {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(token);
        projectEndpoint.clearProjects(request);
        authEndpoint.logout(new UserLogoutRequest(token));
    }

    @Test
    public void testChangeProjectStatusById() throws Exception {
        assertNotNull(project);
        @Nullable final String id = project.getId();
        @NotNull final Status newStatus = Status.IN_PROGRESS;

        @NotNull final ProjectChangeStatusByIdRequest projectChangeStatusByIdRequest = new ProjectChangeStatusByIdRequest(token);
        assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest)
        );
        projectChangeStatusByIdRequest.setId("");
        assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest)
        );
        projectChangeStatusByIdRequest.setStatus(null);
        assertThrows(
                Exception.class,
                () -> projectEndpoint.changeProjectStatusById(projectChangeStatusByIdRequest)
        );
        projectChangeStatusByIdRequest.setId(id);
        projectChangeStatusByIdRequest.setStatus(newStatus);
        @NotNull final ProjectChangeStatusByIdResponse projectChangeStatusByIdResponse = projectEndpoint.changeProjectStatusById(
                projectChangeStatusByIdRequest
        );
        @Nullable final Project actualProject = projectChangeStatusByIdResponse.getProject();
        assertNotNull(actualProject);
        assertEquals(Status.IN_PROGRESS, actualProject.getStatus());
    }

    @Test
    public void testClearProject() throws Exception {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        projectCreateRequest.setName("Project 2");
        projectCreateRequest.setDescription("Project Description 2");
        assertNotNull(projectEndpoint.createProject(projectCreateRequest));
        @NotNull final ProjectListRequest listRequest = new ProjectListRequest(token, ProjectSort.BY_CREATED);
        @Nullable List<Project> projects = projectEndpoint.listProject(listRequest).getProjects();
        assertNotNull(projects);
        assertTrue(projects.size() > 0);

        assertNotNull(projectEndpoint.clearProjects(new ProjectClearRequest(token)));
        @NotNull final ProjectListResponse actualListResponse = projectEndpoint.listProject(listRequest);
        projects = actualListResponse.getProjects();
        assertNull(projects);
    }

    @Test
    public void testCreateProject() throws Exception {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(projectCreateRequest)
        );
        projectCreateRequest.setName("");
        assertThrows(
                Exception.class,
                () -> projectEndpoint.createProject(projectCreateRequest)
        );
        projectCreateRequest.setName("Project 2");
        projectCreateRequest.setDescription("Project Description 2");
        @NotNull final ProjectCreateResponse projectCreateResponse = projectEndpoint.createProject(
                projectCreateRequest
        );
        assertNotNull(projectCreateResponse);
        @Nullable Project actualProject = projectCreateResponse.getProject();
        assertNotNull(actualProject);
    }

    @Test
    public void testGetProjectById() throws Exception {
        assertNotNull(project);
        @NotNull String id = project.getId();
        @NotNull final ProjectGetByIdRequest projectGetByIdRequest = new ProjectGetByIdRequest(token);
        assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(projectGetByIdRequest)
        );
        projectGetByIdRequest.setId("");
        assertThrows(
                Exception.class,
                () -> projectEndpoint.getProjectById(projectGetByIdRequest)
        );
        projectGetByIdRequest.setId(id);
        @NotNull final ProjectGetByIdResponse projectGetByIdResponse = projectEndpoint.getProjectById(projectGetByIdRequest);
        @Nullable final Project actualProject = projectGetByIdResponse.getProject();
        assertNotNull(actualProject);
        assertEquals("Project 1", actualProject.getName());
    }

    @Test
    public void testGetProjectList() throws Exception {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(token);
        projectCreateRequest.setName("Project 2");
        projectCreateRequest.setDescription("Project Description 2");
        assertNotNull(projectEndpoint.createProject(projectCreateRequest));
        @NotNull final ProjectListRequest listRequest = new ProjectListRequest(token, ProjectSort.BY_NAME);
        @Nullable List<Project> projects = projectEndpoint.listProject(listRequest).getProjects();
        assertNotNull(projects);
        assertEquals(2, projects.size());
    }

    @Test
    public void testRemoveProjectById() throws Exception {
        @NotNull final ProjectListRequest listRequest = new ProjectListRequest(token, ProjectSort.BY_CREATED);
        @Nullable List<Project> projects = projectEndpoint.listProject(listRequest).getProjects();
        assertNotNull(projects);
        assertEquals(1, projects.size());
        @NotNull final String id = projects.get(0).getId();
        @NotNull final ProjectRemoveByIdRequest projectRemoveByIdRequest = new ProjectRemoveByIdRequest(token);
        assertThrows(
                Exception.class,
                () -> projectEndpoint.removeProjectById(projectRemoveByIdRequest)
        );
        projectRemoveByIdRequest.setId(id);
        assertNotNull(projectEndpoint.removeProjectById(projectRemoveByIdRequest));
        projects = projectEndpoint.listProject(listRequest).getProjects();
        assertNull(projects);
    }

    @Test
    public void testUpdateProjectById() throws Exception {
        assertNotNull(project);
        @Nullable String id = project.getId();

        @NotNull final ProjectUpdateByIdRequest projectUpdateByIdRequest = new ProjectUpdateByIdRequest(token);
        projectUpdateByIdRequest.setId(id);
        projectUpdateByIdRequest.setName("New Project Name");
        projectUpdateByIdRequest.setDescription("New Description");
        @NotNull final ProjectUpdateByIdResponse projectUpdateByIdResponse = projectEndpoint.updateProjectById(
                projectUpdateByIdRequest
        );
        @Nullable final Project actualProject = projectUpdateByIdResponse.getProject();
        assertNotNull(actualProject);
        assertEquals("New Project Name", actualProject.getName());
        assertEquals("New Description", actualProject.getDescription());

        projectUpdateByIdRequest.setName("");
        assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(projectUpdateByIdRequest)
        );
        projectUpdateByIdRequest.setName(null);
        assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(projectUpdateByIdRequest)
        );
        projectUpdateByIdRequest.setId("otherId");
        projectUpdateByIdRequest.setName("New Project Name");
        assertThrows(
                Exception.class,
                () -> projectEndpoint.updateProjectById(projectUpdateByIdRequest)
        );
    }

}
