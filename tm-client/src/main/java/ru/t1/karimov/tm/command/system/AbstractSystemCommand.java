package ru.t1.karimov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.karimov.tm.api.service.ICommandService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.command.AbstractCommand;
import ru.t1.karimov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @NotNull
    protected ISystemEndpoint getSystemEndpoint() {
        return serviceLocator.getSystemEndpoint();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
