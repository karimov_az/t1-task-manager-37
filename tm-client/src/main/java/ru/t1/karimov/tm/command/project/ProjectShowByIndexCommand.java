package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.project.ProjectGetByIndexRequest;
import ru.t1.karimov.tm.dto.response.project.ProjectGetByIndexResponse;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() -1;
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(getToken());
        request.setIndex(index);
        @NotNull final ProjectGetByIndexResponse response = getProjectEndpoint().getProjectByIndex(request);
        @Nullable final Project project = response.getProject();
        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by index.";
    }

}
